var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT * FROM account WHERE account_id = (?)';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, account) {
        var query = 'CALL account_getschool(?)';
        connection.query(query, queryData, function(err, school) {
            var query = 'CALL account_getcompany(?)';
            connection.query(query, queryData, function(err, company) {
                var query = 'CALL account_getskill(?)';
                connection.query(query, queryData, function(err, result) {
                    result[3] = school;
                    result[4] = company;
                    result[5] = account;
                    callback(err, result);
                });
            });
        });
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO account (email, first_name, last_name) VALUES (?)';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query,[queryData], function(err, result) {
        var account_id = result.insertId;
        var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';
        var accountSchoolData = [];
        if (params.school_id.constructor === Array) {
            for (var i = 0; i < params.school_id.length; i++) {
                accountSchoolData.push([account_id, params.school_id[i]]);
            }
        }
        else {
            accountSchoolData.push([account_id, params.school_id]);
        }
        // NOTE THE EXTRA [] AROUND accountAddressData
        connection.query(query, [accountSchoolData], function(err, result){
            var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';
            var accountCompanyData = [];
            if (params.company_id.constructor === Array) {
                for (var i = 0; i < params.company_id.length; i++) {
                    accountCompanyData.push([account_id, params.company_id[i]]);
                }
            }
            else {
                accountCompanyData.push([account_id, params.company_id]);
            }
            // NOTE THE EXTRA [] AROUND accountAddressData
            connection.query(query, [accountCompanyData], function(err, result){
                var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
                var accountSkillData = [];
                if (params.skill_id.constructor === Array) {
                    for (var i = 0; i < params.skill_id.length; i++) {
                        accountSkillData.push([account_id, params.skill_id[i]]);
                    }
                }
                else {
                    accountSkillData.push([account_id, params.skill_id]);
                }
                // NOTE THE EXTRA [] AROUND accountAddressData
                connection.query(query, [accountSkillData], function(err, result){
                    callback(err, result);
                });
            });
        });
    });

};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE account SET email = ?, first_name = ?, last_name = ? WHERE account_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        //delete account_address entries for this account
        var query = 'DELETE FROM account_school WHERE account_id = ?';
        connection.query(query, queryData[3], function(err, result) {
            var query = 'DELETE FROM account_company WHERE account_id = ?';
            connection.query(query, queryData[3], function(err, result) {
                var query = 'DELETE FROM account_skill WHERE account_id = ?';
                connection.query(query, queryData[3], function(err, result) {
                    var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';
                    var accountSchoolData = [];
                    if (params.school_id.constructor === Array) {
                        for (var i = 0; i < params.school_id.length; i++) {
                            accountSchoolData.push([params.account_id, params.school_id[i]]);
                        }
                    }
                    else {
                        accountSchoolData.push([params.account_id, params.school_id]);
                    }
                    // NOTE THE EXTRA [] AROUND accountAddressData
                    connection.query(query, [accountSchoolData], function(err, result){
                        var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';
                        var accountCompanyData = [];
                        if (params.company_id.constructor === Array) {
                            for (var i = 0; i < params.company_id.length; i++) {
                                accountCompanyData.push([params.account_id, params.company_id[i]]);
                            }
                        }
                        else {
                            accountCompanyData.push([params.account_id, params.company_id]);
                        }
                        // NOTE THE EXTRA [] AROUND accountAddressData
                        connection.query(query, [accountCompanyData], function(err, result){
                            var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
                            var accountSkillData = [];
                            if (params.skill_id.constructor === Array) {
                                for (var i = 0; i < params.skill_id.length; i++) {
                                    accountSkillData.push([params.account_id, params.skill_id[i]]);
                                }
                            }
                            else {
                                accountSkillData.push([params.account_id, params.skill_id]);
                            }
                            // NOTE THE EXTRA [] AROUND accountAddressData
                            connection.query(query, [accountSkillData], function(err, result){
                                callback(err, result);
                            });
                        });
                    });
                });
            });
        });
    });
};

exports.edit = function(account_id, callback) {
    var query = 'SELECT * FROM account WHERE account_id = (?);';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, account) {
        var query = 'CALL account_getschool(?)';
        var queryData = [account_id];
        connection.query(query, queryData, function(err, school) {
            var query = 'CALL account_getcompany(?)';
            var queryData = [account_id];
            connection.query(query, queryData, function(err, company) {
                var query = 'CALL account_getskill(?)';
                var queryData = [account_id];
                connection.query(query, queryData, function(err, result) {
                    result[3] = school;
                    result[4] = company;
                    result[5] = account;
                    callback(err, result);
                });
            });
        });
    });
};