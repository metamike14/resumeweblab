var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT r.*, a.first_name, a.last_name FROM resume_ r ' +
        'LEFT JOIN account a on a.account_id = r.account_id ' +
        'ORDER BY a.first_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getAccountInfo = function(account_id, callback) {
    var query = 'CALL account_getschool(?)';
    var queryData = [account_id];
    connection.query(query, queryData, function(err, school) {
        var query = 'CALL account_getcompany(?)';
        var queryData = [account_id];
        connection.query(query, queryData, function(err, company) {
            var query = 'CALL account_getskill(?)';
            var queryData = [account_id];
            connection.query(query, queryData, function(err, result) {
                result[3] = school;
                result[4] = company;
                callback(err, result);
            });
        });
    });
};




exports.getById = function(resume_id, callback) {
    var query = 'CALL resume_getcompany(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        var query = 'CALL resume_getschool(?)';
        var queryData = [resume_id];

        connection.query(query, queryData, function(err, school) {
            var query = 'CALL resume_getskill(?)';
            var queryData = [resume_id];

            connection.query(query, queryData, function(err, skill) {
                result[2] = school;
                result[3] = skill;
                callback(err, result);
            });
        });
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO resume_ (resume_name, account_id) VALUES (?)';

    var queryData = [params.resume_name, params.account_id];

    connection.query(query, [queryData], function(err, result) {

        var resume_id = result.insertId;
        var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
        var resumeSchoolData = [];
        if (params.school_id.constructor === Array) {
            for (var i = 0; i < params.school_id.length; i++) {
                resumeSchoolData.push([resume_id, params.school_id[i]]);
            }
        }
        else {
            resumeSchoolData.push([resume_id, params.school_id]);
        }
        // NOTE THE EXTRA [] AROUND resumeAddressData
        connection.query(query, [resumeSchoolData], function(err, result){
            var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';
            var resumeCompanyData = [];
            if (params.company_id.constructor === Array) {
                for (var i = 0; i < params.company_id.length; i++) {
                    resumeCompanyData.push([resume_id, params.company_id[i]]);
                }
            }
            else {
                resumeCompanyData.push([resume_id, params.company_id]);
            }
            // NOTE THE EXTRA [] AROUND resumeAddressData
            connection.query(query, [resumeCompanyData], function(err, result){
                var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';
                var resumeSkillData = [];
                if (params.skill_id.constructor === Array) {
                    for (var i = 0; i < params.skill_id.length; i++) {
                        resumeSkillData.push([resume_id, params.skill_id[i]]);
                    }
                }
                else {
                    resumeSkillData.push([resume_id, params.skill_id]);
                }
                // NOTE THE EXTRA [] AROUND resumeAddressData
                connection.query(query, [resumeSkillData], function(err, result){
                    callback(err, resume_id);
                });
            });
        });
    });

};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume_ WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        var query = 'DELETE FROM resume_school WHERE resume_id = ?';
        connection.query(query, queryData, function(err, result) {
            var query = 'DELETE FROM resume_company WHERE resume_id = ?';
            connection.query(query, queryData, function(err, result) {
                var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
                connection.query(query, queryData, function(err, result) {
                    callback(err, result);
                });
            });
        });
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE resume_ SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_id];
    var resume_id = params.resume_id;

    connection.query(query, queryData, function(err, result) {
        //delete resume_address entries for this resume
        var query = 'DELETE FROM resume_school WHERE resume_id = ?';
        connection.query(query, queryData, function(err, result) {
            var query = 'DELETE FROM resume_company WHERE resume_id = ?';
            connection.query(query, queryData, function(err, result) {
                var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
                connection.query(query, queryData, function(err, result) {
                    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
                    var resumeSchoolData = [];
                    if (params.school_id.constructor === Array) {
                        for (var i = 0; i < params.school_id.length; i++) {
                            resumeSchoolData.push([resume_id, params.school_id[i]]);
                        }
                    }
                    else {
                        resumeSchoolData.push([resume_id, params.school_id]);
                    }
                    // NOTE THE EXTRA [] AROUND resumeAddressData
                    connection.query(query, [resumeSchoolData], function(err, result){
                        var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';
                        var resumeCompanyData = [];
                        if (params.company_id.constructor === Array) {
                            for (var i = 0; i < params.company_id.length; i++) {
                                resumeCompanyData.push([resume_id, params.company_id[i]]);
                            }
                        }
                        else {
                            resumeCompanyData.push([resume_id, params.company_id]);
                        }
                        // NOTE THE EXTRA [] AROUND resumeAddressData
                        connection.query(query, [resumeCompanyData], function(err, result){
                            var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';
                            var resumeSkillData = [];
                            if (params.skill_id.constructor === Array) {
                                for (var i = 0; i < params.skill_id.length; i++) {
                                    resumeSkillData.push([resume_id, params.skill_id[i]]);
                                }
                            }
                            else {
                                resumeSkillData.push([resume_id, params.skill_id]);
                            }
                            // NOTE THE EXTRA [] AROUND resumeAddressData
                            connection.query(query, [resumeSkillData], function(err, result){
                                callback(err, result);
                            });
                        });
                    });
                });
            });
        });
    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS resume_getinfo;

     DELIMITER //
     CREATE PROCEDURE resume_getinfo (resume_id int)
     BEGIN

     SELECT * FROM resume WHERE resume_id = _resume_id;

     SELECT a.*, s.resume_id FROM address a
     LEFT JOIN resume_address s on s.address_id = a.address_id AND resume_id = _resume_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL resume_getinfo (4);

 */

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getcompany(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        var query = 'CALL resume_getschool(?)';
        var queryData = [resume_id];

        connection.query(query, queryData, function(err, school) {
            var query = 'CALL resume_getskill(?)';
            var queryData = [resume_id];

            connection.query(query, queryData, function(err, skill) {
                result[2] = school;
                result[3] = skill;
                callback(err, result);
            });
        });
    });
};