var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');


// View All account
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

// View the account for the given id
router.get('/', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.getById(req.query.account_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('account/accountViewById', {'result': result[5][0],
                   school: result[3][0],
                   company: result[4][0],
                   skill: result[0]});
           }
        });
    }
});

// Return the add a new account form
router.get('/add', function(req, res){
    school_dal.getAll(function(err,school) {
    if (err) {
        res.send(err);
    }
    else {
        company_dal.getAll(function(err,company) {
            if (err) {
                res.send(err);
            }
            else {
                skill_dal.getAll(function(err,skill) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('account/accountAdd', {'school': school, 'company': company, 'skill': skill});
                    }
                });
            }
        });
    }
});
});

// View the account for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email == null) {
        res.send('Email is required.');
    }
    else if(req.query.first_name == null) {
        res.send('Account must have a first name');
    }
    else if(req.query.last_name == null) {
        res.send('Account must have a last name');
    }
    else if(req.query.school_id == null) {
        res.send('Account schools');
    }
    else if(req.query.company_id == null) {
        res.send('Account companies');
    }
    else if(req.query.skill_id == null) {
        res.send('Account skills');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('A account id is required');
    }
    else {school_dal.getAll(function(err,school) {
        if (err) {
            res.send(err);
        }
        else {
            company_dal.getAll(function(err,company) {
                if (err) {
                    res.send(err);
                }
                else {
                    skill_dal.getAll(function(err,skill) {
                        if (err) {
                            res.send(err);
                        }
                        account_dal.edit(req.query.account_id, function(err, result){
                            res.render('account/accountUpdate', { account: result[5][0],
                                'full_school': school,
                                'full_company': company,
                                'full_skill': skill,
                                school: result[3][0],
                                company: result[4][0],
                                skill: result[0]});
                        });
                    });
                }
            });
        }
    });

    }

});

router.get('/edit2', function(req, res){
   if(req.query.account_id == null) {
       res.send('A account id is required');
   }
   else {
       account_dal.getById(req.query.account_id, function(err, account){
           address_dal.getAll(function(err, address) {
               res.render('account/accountUpdate', {account: account[0], address: address});
           });
       });
   }

});

router.get('/update', function(req, res) {
    account_dal.update(req.query, function(err, result){
       res.redirect(302, '/account/all');
    });
});

// Delete a account for the given account_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
         account_dal.delete(req.query.account_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/account/all');
             }
         });
    }
});

module.exports = router;
