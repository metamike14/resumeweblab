var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');


// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

// View the resume for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('resume/resumeViewById', {result: result[0], school: result[2][0], company: result[0], skill: result[3][0] });
           }
        });
    }
});

// Return the add a new resume form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getAccountInfo(req.query.account_id, function(err,info) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAdd', {
                account: req.query.account_id,
                school: info[3][0],
                company: info[4][0],
                skill: info[0]
            });
        }
    });
});

router.get('/add/selectuser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeSelectUser', {'account': result});
        }
    });
});

// View the resume for the given id
router.post('/insert', function(req, res){
    // simple validation
    if(req.body.resume_name == null) {
        res.send('resume Name must be provided.');
    }
    else if(req.body.school_id == null) {
        res.send('At least one school must be selected');
    }
    else if(req.body.company_id == null) {
        res.send('At least one company must be selected');
    }
    else if(req.body.skill_id == null) {
        res.send('At least one skill must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.body, function(err,resume_id) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                resume_dal.getAccountInfo(req.body.account_id, function(err,info) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        resume_dal.edit(resume_id, function (err, result) {
                            res.render('resume/resumeUpdate', {
                                resume: result[0][0],
                                school: result[2][0],
                                company: result[0],
                                skill: result[3][0],
                                full_school: info[3][0],
                                full_company: info[4][0],
                                full_skill: info[0],
                                was_successful: true
                            });
                        });
                    }
                });
            }
        });
    }
});


router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    school_dal.getAll(function(err,school) {
        if (err) {
            res.send(err);
        }
        else {
            company_dal.getAll(function (err, company) {
                if (err) {
                    res.send(err);
                }
                else {
                    skill_dal.getAll(function (err, skill) {
                        if (err) {
                            res.send(err);
                        }
                        else {
                            resume_dal.edit(req.query.resume_id, function (err, result) {
                                res.render('resume/resumeUpdate', {
                                    resume: result[0][0],
                                    school: result[2][0],
                                    company: result[0],
                                    skill: result[3][0],
                                    full_school: school,
                                    full_company: company,
                                    full_skill: skill
                                });
                            });
                        }
                    });
                }
            });
        }
    })
});

router.get('/edit2', function(req, res){
   if(req.query.resume_id == null) {
       res.send('A resume id is required');
   }
   else {
       resume_dal.getById(req.query.resume_id, function(err, resume){
           address_dal.getAll(function(err, address) {
               res.render('resume/resumeUpdate', {resume: resume[0], address: address});
           });
       });
   }

});

router.get('/update', function(req, res) {
    resume_dal.update(req.query, function(err, result){
       res.redirect(302, '/resume/all');
    });
});

// Delete a resume for the given resume_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
         resume_dal.delete(req.query.resume_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/resume/all');
             }
         });
    }
});

module.exports = router;
